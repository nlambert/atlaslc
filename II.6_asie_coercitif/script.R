library(sf)
library(mapsf)
library(rnaturalearth)
library(rmapshaper)
library(readxl)

# Map preparation, world by subregions
reg <- read.csv("countries_en.csv", sep = ",")
reg <- reg[,c(1:4)]
colnames(reg) <- c("adm0_a3", "name",  "sub1", "sub2")

# Map preparation
ori <- ne_countries(scale = 50, type = "countries", returnclass = "sf")
ori <- ori[,c("iso_a2", "iso_a3", "adm0_a3", "admin", "subregion", "un_a3", "pop_est")]
ori <- ori[!ori$adm0_a3 %in% c("ATA", "ATF", "KAS"),]

cntry <- ms_simplify(ori, keep = .05)

crs <- "+proj=bertin1953"
cntry <- st_transform(cntry, crs = crs)

cntry <- merge(cntry, reg[,c(1,4)], by = "adm0_a3", all.x = TRUE)

reg <- aggregate(cntry[,c("sub2")], 
                   by = list(ID = cntry$sub2), 
                   FUN = min, simplify = TRUE)


reg_df <- read.csv("stats_subregion.csv", sep = ",")
reg <- merge(reg, reg_df, by = "sub2", all.x = TRUE)

sphere <- st_convex_hull(st_union(reg))
grat <- st_graticule(reg)
sphere <- st_convex_hull(st_union(grat))
sea_b <- st_union(reg)

mf_map(sphere, col = "#A7C1DC")
mf_map(reg, type = "typo", var = "sub2", border = "white", leg_pos = "n", add = TRUE)
mf_map(sea_b, col = NA, border = "darkblue", add = TRUE) 
mf_map(x = reg, var = "WORKERS_2019", type = "prop", col = "lightblue", border = "white", inches = .5,
       val_max = max(reg$WORKERS_2019), leg_pos = "n")

mf_map(x = reg, var = "WORK_MIGR_2019", type = "prop", col = "brown 1", border = "white", inches = .5,
       val_max = max(reg$WORKERS_2019), leg_pos = "n")

mf_legend(val = c(min(reg$WORK_MIGR_2019), 100, 500, max(reg$WORKERS_2019)), col = "lightgrey",
          type = "prop", pos = "bottomright", inches = .5, title = "Nombre de travailleurs en 2019")

mf_legend(val = c("Ensemble des travailleurs", "Travailleurs migrants"), pal = c("lightblue", "brown 1"),
          type = "typo", pos = "bottomleft")

mf_layout(title = "169 millions de travailleurs migrants = 20.6 % des travailleurs dans le monde", credits = "Source : ILO Global Estimates on International Migrant Workers – Results and Methodology – Third edition, 2021.",
          arrow = FALSE, scale = FALSE)



# Map preparation - Asia
ori <- ms_simplify(ori, keep = .3)

# Map 1 - Intra South - Eastern Asia
#crs <- 6933
crs <- "+proj=natearth2 +lon_0=0" 
crs <- "+proj=ortho +lon_0=110"
ori <- st_transform(ori, crs = crs)
sel <- ori[ori$subregion == "South-Eastern Asia",]
bbox <- st_bbox(sel)


sel2 <- rbind(sel, ori[ori$iso_a3 %in% c("IND", "BGD", "CHN", "HKG", "BTN", "PNG", "TWN", "AUS", "NPL"),])

bbox <- st_as_sfc(st_bbox(bbox + c(-1200000, -600000, 400000, 200000)))
sel2 <- st_intersection(sel2, bbox)
bg <- st_union(sel2)
grat <- st_graticule(bg)

# Manage data
df <- data.frame(read_xlsx("undesa_pd_2020_ims_stock_by_sex_destination_and_origin.xlsx",
                           skip = 10, sheet = "Table 1"))
df <- df[,c(4, 7, 14)]
colnames(df) <- c("j", "i", "MIGR_2020")

for (i in 1:nrow(df)) {
  
  if (nchar(df$i[i]) == 2) {
    
    df$i[i] <- paste0("0", df$i[i])
    
  } 
}

for (i in 1:nrow(df)) {
  
  if (nchar(df$j[i]) == 2) {
    
    df$j[i] <- paste0("0", df$j[i])
    
  }
}

df <- df[df$j %in% levels(as.factor(sel2$un_a3)),]
df <- df[df$i %in% levels(as.factor(sel2$un_a3)),]
df$MIGR_2020 <- df$MIGR_2020 / 1000
df <- df[df$MIGR_2020 > 100,]

# Origin - destination
selij <- merge(sel2, df, by.x = "un_a3", by.y = "i", all.x = TRUE) 
selij <- mf_get_links(sel2, df, x_id = "un_a3", df_id = c("i", "j"))

mf_map(bbox, col = "#A7C1DC", border = NA)
mf_map(sel2, col = "lightgrey", border = "white", add = TRUE)

mf_map(sel, col = "peachpuff", border = "white", add = TRUE)

mf_map(selij, var = "MIGR_2020", type = "prop", add = TRUE, col = "red",
       leg_pos = "n", val_max = max(selij$MIGR_2020), lwd_max = 15) 

mf_map(bg, col = NA, border = "darkblue", lwd = .2, add = TRUE)

mf_legend(val = c(101, 350, 1132, max(selij$MIGR_2020)), col = "red", lwd = 15,
          type = "prop_line", pos = "bottomright", title = "Flux de migration 2020\n(milliers d'individus)")

mf_label(sel, var = "admin")
mf_map(bbox, col = NA, border = "black", add = TRUE)
mf_scale(pos = "bottomleft", size = 500)
mf_map(grat, col = "white", add = TRUE)

#
df <- data.frame(read_xls("population_2020.xls",
                           skip = 3, sheet = "Data"))

sel2 <- merge(sel2, df[,c("Country.Code", "X2020")], by.x = 'iso_a3', by.y = "Country.Code", all.x = TRUE)
sel2$X2020 <- sel2$X2020 / 1000000

mf_map(sel2, var = "X2020", type = "prop", leg_pos = "n", add = TRUE)

mf_legend_p(val = c(1, 100, 500, 1402), col = "red", inches = .3, pos = "topright")


# Statistics
df <- data.frame(read_xlsx("undesa_pd_2020_ims_stock_by_sex_destination_and_origin.xlsx",
                           skip = 10, sheet = "Table 1"))
df <- df[,c(4, 7, 14)]
colnames(df) <- c("j", "i", "MIGR_2020")

for (i in 1:nrow(df)) {
  
  if (nchar(df$i[i]) == 2) {
    
    df$i[i] <- paste0("0", df$i[i])
    
  } 
}

for (i in 1:nrow(df)) {
  
  if (nchar(df$j[i]) == 2) {
    
    df$j[i] <- paste0("0", df$j[i])
    
  }
}
df <- df[df$j %in% levels(as.factor(sel2$un_a3)),]
df <- df[df$i %in% levels(as.factor(sel2$un_a3)),]
df$MIGR_2020 <- df$MIGR_2020 / 1000
selij <- merge(sel2, df, by.x = "un_a3", by.y = "i", all.x = TRUE) 
selij <- mf_get_links(sel2, df, x_id = "un_a3", df_id = c("i", "j"))
selij <- st_set_geometry(selij, NULL)
ase <- c("458", "764", "702", "116", "104", "704", "418", "360", "608",
         "622", "096")
selij <- selij[selij$j %in% ase,]
tot <- sum(selij$MIGR_2020)
test <- aggregate(selij$MIGR_2020, by = list(selij$j), FUN = sum)
test$rt <- test$x / tot * 100

ase1 <- sel2[sel2$un_a3 %in% ase,]
ase1$POP_RT <- ase1$X2020 / sum(ase1$X2020) * 100

tes(test1$pop_est) / sum(sel$pop_est) * 100
sum(test2$MIGR_2020) / sum(selij$MIGR_2020) * 100 


# Macro - region flows
# Map preparation
ori <- ne_countries(scale = 50, type = "countries", returnclass = "sf")
ori <- ori[,c("iso_a2", "iso_a3", "adm0_a3", "admin", "subregion", "un_a3", "pop_est")]

# Delete Groeland
ori <- ori[!ori$adm0_a3 %in% c("GRL", "ATA"),]

reg <- read.csv("countries_en.csv", sep = ",")
reg <- reg[,c(1,9)]
colnames(reg) <- c("adm0_a3", "UN_REGION")
ori <- merge(ori, reg, by = "adm0_a3", all.x = TRUE)

reg <- aggregate(ori[,c("UN_REGION", "pop_est")], 
                 by = list(ID = ori$UN_REGION), 
                 FUN = sum, simplify = TRUE)

reg <- ms_simplify(reg, keep = .05)
reg$UN_REGION <- as.character(reg$UN_REGION)
reg$pop_est <- reg$pop_est / 1000000

# Données de flux
df <- data.frame(read_xlsx("undesa_pd_2020_ims_stock_by_sex_destination_and_origin.xlsx",
                           skip = 10, sheet = "Table 1"))
df <- df[,c(4, 6:7, 14)]
colnames(df) <- c("j", "NAME_UN_REGION", "i", "MIGR_2020")
df$i <- as.character(df$i)
df$j <- as.character(df$j)

df <- df[df$i %in% levels(as.factor(reg$ID)),]

# Retirer pays
sel <- levels(as.factor(reg$ID))
df <- df[df$j %in% sel,]

# Ne garder que les flux d'asie du sud-est
df1 <- df[df$i == "920",]
df2 <- df[df$j == "920",]
df <- rbind(df1, df2)

selij <- mf_get_links(reg, df, x_id = "ID", df_id = c("i", "j"))
selij$MIGR_2020 <- selij$MIGR_2020 / 1000000
selij <- selij[selij$MIGR_2020 > 0.1,]

# Reprojection  
#prj <- "+proj=eqdc +lat_0=-85 +lon_0=106 +lat_1=-85 +lat_2=-85 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs" 
prj <- "+proj=eqdc +lat_0=-20 +lon_0=146 +lat_1=-20 +lat_2=-20 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs" 
reg <- st_transform(reg, prj)
selij <- st_transform(selij, prj)


sphere <- st_convex_hull(st_as_sfc((st_bbox(reg +  c(-10000,-10000,10000,10000)))))

bbox <- st_as_sfc(st_bbox(reg + c(-10000, -10000, 10000, 10000), crs = prj))
grat <- st_graticule(bbox, margin = c(+100, +100, +100, +100))


grat <- st_graticule(
  x = reg,
  lon = NULL,
  lat = NULL,
  ndiscr = 100,
  margin = 20
)

mf_map(bbox)
mf_map(grat, add = TRUE)



grat <- st_graticule(sphere)
sphere <- st_convex_hull(st_union(grat))
sea_b <- st_union(reg)

mf_map(sphere, col = "#A7C1DC")
mf_map(reg, type = "typo", var = "sub2", border = "white", leg_pos = "n", add = TRUE)

mf_map(reg, col = "peachpuff", border = "white", add = TRUE)
mf_map(sea_b, col = NA, border = "darkblue", add = TRUE) 

mf_map(selij, var = "MIGR_2020", type = "prop", add = TRUE, col = "red",
       leg_pos = "topleft", val_max = max(selij$MIGR_2020), lwd_max = 30) 

mf_legend(val = c(0.1, 1, 2, max(selij$MIGR_2020)), col = "red", lwd = 30, val_rnd = 2,
          type = "prop_line", pos = "bottomright", title = "Flux de migration 2020\n(milliers d'individus)")

mf_map(reg, var = "pop_est", type = "prop")
mf_map(grat, col = "black", lwd = .2, add = TRUE)

#### Data preparation
# df <- read.csv("MST_TEAP_SEX_EDU_CCT_NB_A-filtered-2022-01-27.csv", sep = ",")
# years <- levels(as.factor(df$time))
# df <- df[,c(1, 5, 6, 8)]
# colnames(df)[1] <- "iso_a3"
# 
# levels(as.factor(df$classif1))
# levels(as.factor(df$classif2))
# 
# df1 <- df[df$classif1 == "EDU_AGGREGATE_ADV" &
#             df$classif2 == "CCT_CIT_NONCIT",]
# 
# colnames(df1)[4] <- "NONCIT_ADV"
# df1 <- df1[,c(1,4)]
# 
# df2 <- df[df$classif1 == "EDU_AGGREGATE_ADV" &
#             df$classif2 == "CCT_CIT_TOTAL",]
# colnames(df2)[4] <- "CITTOT_ADV"
# 
# df3 <- df[df$classif1 == "EDU_AGGREGATE_BAS" &
#             df$classif2 == "CCT_CIT_NONCIT",]
# colnames(df3)[4] <- "NONCIT_BAS"
# 
# df4 <- df[df$classif1 == "EDU_AGGREGATE_BAS" &
#             df$classif2 == "CCT_CIT_TOTAL",]
# colnames(df4)[4] <- "CITTOT_BAS"
# 
# df5 <- df[df$classif1 == "EDU_AGGREGATE_INT" &
#             df$classif2 == "CCT_CIT_NONCIT",]
# colnames(df5)[4] <- "NONCIT_INT"
# 
# df6 <- df[df$classif1 == "EDU_AGGREGATE_INT" &
#             df$classif2 == "CCT_CIT_TOTAL",]
# colnames(df6)[4] <- "CITTOT_INT"
# 
# df7 <- df[df$classif1 == "EDU_AGGREGATE_LTB" &
#             df$classif2 == "CCT_CIT_NONCIT",]
# colnames(df7)[4] <- "NONCIT_LTB"
# 
# df8 <- df[df$classif1 == "EDU_AGGREGATE_LTB" &
#             df$classif2 == "CCT_CIT_TOTAL",]
# colnames(df8)[4] <- "CITTOT_LTB"
# 
# head(df8)
# 
# df <- merge(df2[,c(1,4)], df1, by = "iso_a3", all.x = TRUE)
# df <- merge(df, df3[,c(1,4)], by = "iso_a3", all.x = TRUE)
# df <- merge(df, df4[,c(1,4)], by = "iso_a3", all.x = TRUE)
# df <- merge(df, df5[,c(1,4)], by = "iso_a3", all.x = TRUE)
# df <- merge(df, df6[,c(1,4)], by = "iso_a3", all.x = TRUE)
# df <- merge(df, df7[,c(1,4)], by = "iso_a3", all.x = TRUE)
# df <- merge(df, df8[,c(1,4)], by = "iso_a3", all.x = TRUE)
# 
# write.csv(df, "data_consolidated.csv",  row.names = FALSE)

# df2 <- read.csv("data_consolidated.csv", sep = ",")
# 
# sel <- merge(sel, df2, by = "iso_a3", all.x = TRUE)
