# Réunion Atlas LC

La Cimade, 24 nov 2012
---

### Intro générale

Mettre une carte par les visas ???

### Formes historiques

**cuouc**

Logiques de domination, contexte colonial. Partie un peu en dificulté.

- I.1 intro

OC : Carto des empires coloniaux. Circulation des européens et des indigènes ?
Supperposition avec les espaces de libre circulation actuels ?

Ajouter la couche population de Ronan.

+ injecter la question de la liberté de circulation.

Présence des populations * schemas coloniaux.

(Ronan : localisation des populations en 1 500 ?)

Reflexion : mettre en avant les routes plutôt que les frontières.

- I.2 Drame de sidi Ferruch

Proposition OC : 1/2 page au format A4 pour illustrer le texte. Puis, une chronologie pour voir comment ce drame se ressitue dans l'histoire entre l'Algérie et la France.

- I.3 Circulation dans empire colonial belge

Anna : Peut être une photo plutôt qu'une carte ?

Sarah : Mobilité de soldats coloniaux. Déplacements forcés de population pour des conquetes... Les civilisés. Comment le statut te donne droit à la mobilité. Statut intermédiare entre citoyen belge et collonisé. A voir...

OC : 1/2 page de carto classique sur les empires coloniaux qui étaient des espaces de libre circulation pour les colons.

- I.4 Liberté de circulation en Afrique de l'ouest avant la colonisation

Besoin de dialogue avec l'autrice du texte.

- I.5 La Mecque

David

- I.6 femmes

Comment au 19e debut 20 au RU, ils ont envoyé des femmes pour aller dans les colonies de peuplement, là où il n'y a que des hommes. Question de classe sociale. Question matrimoniale.

- I.7 Libye.

- I.8 Circulation ety conquete aux USA.

Un siècle de déshonneur. Maniere dont les colons signent des traités avec les tribus et qui conduisent à des déplacement forcés de populations indiennes.
Sioux + autre tribus => 2 cartographies pour 2 tribus.

- I.9 L'esclave qui deveint millionaire

Ca roule....

- I.10 liberté de circu pensée par la première internationale

L'auteur nous a laché.

-> Tenter avec Grégory Mauzé.

Comment des personnes clé circulent à cette époque là.

Cf livre Un siecle d'anarchisme de "Fredo monsediona"

- I.11 pandémies

FB : a un atlas sur les pandémies.

- I.12 chagement climatique

Françoise Gémenne. OC le relance.

Carto : frise chronologique ?

### Formes de liberté de circulation

Différentes formes dans le Monde aujourd'hui. Attaquer ca par des angles précis. e.g. la liberté de circu des travailleurs. D'autres espaces de façon plus ransversale.
Espaces d'integration régionaux qui ont pris en compte la lobre circulation des personnes. Qu'est ce qui a été pensé d'un cadre politique et juridique pour la liberté de circu. Multi ou bilateral.

- II.1 intro

Faire cohabiter les différentes zones.

OC : potentiels franchissements de frontires à l'intérieur de ces zones + franchissements de frontiere exterieurs à ces zones.

Reprendre la carte de Thomas + Prendre les données mondiales pays * pays et comparer les données inter et intra. Barres ? Cercles ? Camenberts ?

- II.2 liberté de circulation et protection sociale en europe

Au début travailleurs

OC :

2 cartes format A5 paysage.

1 - montrer la circulation qui s'opère en Europe.
27 * 27 pays....
Ou atractivité des pays ?
Ou principaux flux
Données Eurostat ? Données UN ?

2 - ????

Retractation de la liberté de circulation des personnes.

- II.3 Adhesion Bulgarie et Roumanie

Mesures transitoires. Tensions entre motivation protectionisme, racisme contre les roms.

-> Manu Héelio : Rester sur une échelle européenne

-> Dans un contexte où ces ressortissants pouvaient circuler librement, la seule marge d'action des états est de leur limiter l'accès au marché du travail. C'ets ce qu'ont fait le plus longtemps possible la france et la roumanie => aller voir quand chaque état a levé les restrictions. E.g. le RU l'a fait directement. La France et l'Italie ont trainé.

- II.4 travailleurs détachés sur de la France.

Deux temporalités. temps court : Flex worker vs temps long (maintenir les étrangers) => 2 recits. Externalisation des couts sociaux du travail.

- II.5 vivre en famille

L'Europe contribue à élargir le droit à la mobiloité via le droit de vivre en famille. La loberté de circulation en Europe implique des familles non européennes dans ce droit à la mobilité. Question des ressortissants d'états tiers => les états membres sont frileux et essayent de limiter.
Pour une fois, l'UE a vraiment créé du droit et des droits... "En France, il vaut mieux de marier avec un roumain qu'avec un français" ????

- II.6 Asie du sud-est

Circulation des travaillleurs dans l'ASEAN. Accords bilateraux de main d'oeuvre entre différents états. Singapour, Malaisie dominent dans la région. Politiques pour attirer des capitaux. Développent une pollitique pour attirer des travailleurs migrants en enlevant tout droit aux travailleurs détachés. Mise en circulation associée à zero droit.
Mettre en avant les flux de travailleurs migrants et/ou capitaux
Rendre compte de la limite des droits.

OC : replacer la question de la mobilité des travailleurs dans la mobilité en général.

Evolution du PIB vs vulnérabilité des travailleurs migrants (morts des travailleurs migrants). idée : Cf https://ilostat.ilo.org/.

- II.7 LC dans l'espace post soviétique

Apres chute de l'URSS. Question économique articulée à la question du droit à circuler. Pendant l'URSS, c'était tellement pas cher qu'on pouvait aller le we à Moscow. Cela devient cher après.

-> carto olga ?
OC en backup avec réserve.

- II.8 Tunisie-Cote d'ivoire

?????

- II.9 Agropastoralisme CDAO

Libre circulation des éleveurs pendant leur transumance. Pays sahéliens vers les pays côtiers. On peut visualiser ces mouvement saison seche saison humide et dans le contexte securitaire d ela CDAO et contexte de changement climatique.

- II.10 biométrie

Faciliter la circulation dans l'espace de la CDAO. Enjeux autour du developpement de la biométrie pour faciliter la mobilité sans remettre en question le régime des frontières. **AFIS**, système des systèmes qui permet l'interoparbilité des bases de données. + faire apparaitre les acteurs qui gravitent autour de ça.

- II.11 Mercosur

Espace de libre circulation créé pour palier à la la migration de travail irrégulière.

- II.12 Accord C4, amérique du sud

Frontère verticale
Espace de transit vers espace de blocage.

### Marchandises (tout sauf les humains)

Circulation des marchandises et mettre en regard avec la circulationd es hommes. E.g. les fruits rouges. Question de la main d'oeuvre saisonnière. Call centers au mexique. Circulation des capitaux. Les marins. Les données. Systhèse sur le champignon de la fin du monde. Texte sur le verre à venir. Cas des bracelets indiens...

- III.1 Introduction

Reka

- III.2 Circulation des produits agricoles

Cartographier le boycot

- III.3 chemins du verre

????

- III.4.infrastructures de travailleurs

transport par containers + condition de travail des marins. Ils passent à travers les frontières car ils lont une circulation facilitée parce que ce sont de stravailleurs et que leur travail sert. C'est la fonction qui permet de renverser des hierachies. Une fois au sol, il redevient immobile.

Carto -> circulation des containers + situation de marin (travaux de françois Lille ?)

- III.5 Champigon de la fin du monde

Moi

- III.6 Call centers au Mexique

Ok

- III.7 capitaux

Reka

- III.8 Circulation des Données

Carte sur l'open acces, idéologie du libre.

### Liberté de circulation en actes (ISS, Heller, ...)

Prtaiques et mobilisations pour la liberté de circulationdLibre franchissement des frontiere. Accès aux territoires et à la protection sociale. Caravanes, personnes trans, ...

- IV.1 Introduction

Carto sur les mobilisation qui ont emerger dans le contexte du Covid19. Comités de sans papiers, marche de solidarité... Carte de Sarah


- IV.2 formes de protections sociales transnationales autonomes

Comment les pays d'origine s'organisent pour assurer un systeme de protection sociale à ceux qui sont parti.

Tony

- IV.3 Auto organiser des transferts d'argent.

Transferts d'argent qui ne passe pas par western union.
OC a des données de 2005 à 2015

Idée de la carte : droit à mobiliser son argent sans être taxé. Transferer des biens et des moyens. Gestion autonome des remises.

Carte avec données de transfert par Western union sur une demi page.

Carte sur le commerce à la valise (début des années 90 entre la france et l'algérie). Transfert d'argent par valise. Banquier de l'emigré. Partir d'exemples individuels. 2 exemples de circulation informelle. Transfert de documents ou de valise.

Quelles données ? Vor avec les auteurs.

- IV.4 Genre à Ceuta et Melilla

Comment les hommes et les femmes performent le franchissement de frontière. Comment les femmes jouent de ces représentations stéréotypées pour permettre le franchissement.

- IV.5 Caravanes de migrants en amérique centrale

E Helio a des cartes de trajectoires animées.

- IV.6 trans : mobilisation + acces aux soins

Lucie

- IV.7 techno : peut-on hacker les frontières ?

Comment peut-on attaquer les nouvelles techno pour passer.

Un peu low tech :-)

-> document comment passer les frontières fait par un migrant aux archives du GISTI ?

- IV.8 Elaboration de cartographies pour subjectiver les trajectoires

Il y a déjà une carte

- IV.9 empreintes digitales

En attente...

- IV.10 monetisation : passeurs

David
OC : Faire un glossaire ? La manière dont on nomme les passeurs.

- IV.11 délinquants solidaires

Briançonais. Petits gestes de solidarité qui permettent de circuler.
Avec Morganne, Vintimille, Menton
=> sur la frontiere franco italienne.

Actes ordinaires.

Cartographier les refuges.
fleches : prendre les gens en stop, etc.
Bidon d'eau aux usa, mexique.
Tous ce spetits gestes
les balises des chemins de randonnées par les no border.

- IV.12 Alarme phone

Morgane
traces GPS ?

- IV.13 balkans

Morgane

### Imaginaires contradictoires de la liberté de circulationd

Répondre à la critique de gauche de la liberté de circulation.

- V.I

- V.II anarchisme

OC : lien entre les classiques (e.g. Reclus) et les No borders à creuser. Et lien avec la partie historique du début.

Pour la carto, et si on sollicitait Raph ? Et pourquoi pas le mobiliser sur l'ensemble de la partie.

- V.III gestion modiale des migrations

Peut être moins dans l'imaginaire. Discours déjà constituté.

E Helio : carte avec des pictogrammes que eux utilisent pour faire la pub de ces formes de circulation circulaires.

Représentation des ces formes logistiques "lisses". Les "petits duplo". Isotypes. Représentation par des formes symboliques.

OC : On peut avoir 2 types de carto. Une carto imaginaire subversive + une 2e qui soit "je sais pas"

- V.4 Option libérale

On est plus dans l'Imaginaire

David : si ca se passait comme ca, ou iraient les gens

EH : Mobilisations patronales ?

Lister différentes déclarartions

NL : Quelle quantité de population ou de richesse il faudrait transferer pour equirepartition de PIUB par habitant.

- V.5 Ecolo

Pas de nouvelles de françoise Gemene

- V.6 local

Mise en lien de ports et de villes avec de vrais engagements.

- V.7 approche décoloniale

Passeports indigènes.

  - V.8 conception féministe

Texte collectif.
Sarah B intéressée. Veut faire un atelier participatif.
Mettre en contact avec Laurence qui était aussi intéréssée.

- V.9 post coloniale

Carte upside down (southup).
Faire le bilan d'un déséquilibre.
Aide au développement et remise ?
Question des passeprts, visas
Lien entre les passeports et les pays colonisés.

- V.10 peuples nomades

Tziganes, roms,...
mobilités pour travail, famille + modes de vie.

### Conclusion

A élaborer.

___

### TODO NL

- I.1. Avec RY. carte des empires coloniaux (+ circulations indigenes ?)
Ajouter la couche population de Ronan.
injecter la question de la liberté de circulation.
Mettre en avant les routes plutôt que les frontières.

- I.10. Avec OC. En chantier. Pas d'auteur pour l'instant

- II.1 Avec FB. Carte des zones de libeté de circulation. Comparer flux inter et flux intra.

- II.2 2 cartes format A5 paysage. 1 - montrer la circulation qui s'opère en Europe.
27 * 27 pays....
Ou atractivité des pays ?
Ou principaux flux
Données OCDE ? Données UN ?
2 - Carte à l'échelle des subregions mondiales pour comparer flux intra europ et flux depuis et vers l'Europe.
Les deux cartes forment ainsi une représentation à 2 échelles.

Cf données eurostat sur la protection sociale.

- II.6 Asie du sud-est avec RY et FB

- II.10 Faciliter la circulation dans l'espace de la CDAO. Enjeux autour du developpement de la biométrie pour faciliter la mobilité sans remettre en question le régime des frontières. **AFIS**, système des systèmes qui permet l'interoparbilité des bases de données. + faire apparaitre les acteurs qui gravitent autour de ça.

- III.2 Avec FB. Circulation des produits agricoles. La banane ? La tomate ? Données FAO. Emanuelle Helio reflechit à la carte.

- III.5 Champigon de la fin du monde
Faire en carte en fonction de ce qu'on a.

- IV.3 faire aussi cryptomonnaies

-V.5 option libérale : Quelle quantité de population ou de richesse il faudrait transferer pour equirepartition de PIUB par habitant. Cf MTA manuel carto

Pour l'équilibre, il faudreait transfere xxxx $ chaque année. OU déplacer xxxxx personnes.
