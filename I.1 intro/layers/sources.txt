library.thinkquest.org/
World Historical GIS Data: 2000 BCE to 1994 CE
While the site is now defunct, Oracles, 
Thinkquest.org site consolidated a series of country boundary data into shapefiles. 
 Created as an educational site in 1996 and acquired by Oracle in 2002, the site went defunct in 2013.
 Thanks to the Wayback Machine, the shapefiles of country boundaries spanning between 2000 BCE and 1994 CE
 can still be downloaded.  The data itself should be used with caution and only for small scale projects. 
 Created by students, the archived disclaimer page explains that the data has a spatial error of 
roughly +/- 40 miles and the best available information, especially for the oldest years, is not the most reliable.

# Marine regions
https://www.marineregions.org/downloads.php

# Routes historiques
http://www.ciolek.com/OWTRAD/DATA/oddda.html

# Routes historiques 11th - 12th siècle
https://imgur.com/MsXaOdV